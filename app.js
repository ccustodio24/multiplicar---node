const argv = require('./config/yargs').argv;
const colors = require('colors');

////obtengo todo lo enviado en archivo multplicar por el metodo module
const {crearArchivo, listar} = require('./multiplicar/multiplicar');//destructuracion



let comando = argv._[0]

switch (comando) {
	case 'listar':
		listar(argv.base,argv.limite)
		break;
	case 'crear' : 
	//desde la console se escribe node app --base=5
		crearArchivo(argv.base, argv.limite)
		.then(archivo => console.log(`Archvo creado :`,colors.green(archivo)))
		.catch(error => console.log(error));		
		break;

	default:
		console.log('Comando no conocido')
		break;
}
//DEPENDENCIAS UTILIZADAS
//npm install colors --save
// npm i--save yars

//console.log(argv.base);

// let parametro = argv[2];
// let base = parametro.split('=')[1]//convierte un string en array


