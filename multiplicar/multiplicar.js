// Require
const fs = require('fs');//paquete nativo de node
const colors = require('colors');
//const fs = require('express');//paquetes no nativos de node
//const fs = require('fs');//paquetes que creamos nosotros

let listar = (base, limite=10) => {
	console.log('====================='.green);
	console.log(`Tabla de ${base}`.green);
	console.log('====================='.green);
	for (let i = 1; i <= limite; i++) {
		 console.log(`${base} * ${i} = ${base * i}`)
	}
}

let crearArchivo = (base,limite=10) => {
	return new Promise((resolve,reject) => {
		if (!Number(base)) {
			reject(`El valor introducido ${base} no es un numero`);
			return;//para romper el ciclo
		}
		let data = '';
		
		for (let i = 1; i <= limite; i++) {
			data += `${base} * ${i} = ${base * i}\n`
		}
		fs.writeFile(`tablas/tabla- ${base}-al-${limite}.txt`, data, (err) => {
			if (err) 
				reject(err)
			else 
				resolve(`tabla-${base}-al-${limite}.txt`);
		});
	})
}

//Module : es un metodo global 
//console.log(module)
module.exports = {
	crearArchivo,
	listar
}